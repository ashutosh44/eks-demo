
variable "env" {
  description = "env to deploy into, should typically ab_dev/ab_qa/ab_prod"
  default = "ab_dev"
}

###### Variable for Region ##########
variable "infra_region" {
    description = "region for the infra"
    default = "ap-south-1"
}
variable "key_credentials_path" {
    description = "key credentials path"
    default = "/home/opstree/.aws"
}
variable "vpc_id" {
    description = "VPC ID"
    default = "vpc-03abfa52e737dd811"
}

variable "eks_pvt_subnet_az_a" {
  description = "eks pvt subnet az a id which was craeted in base infra code"
  default = "subnet-00c168e2059131389"
  
}
variable "eks_pvt_subnet_az_b" {
  description = "eks pvt subnet az b id which was craeted in base infra code"
  default = "subnet-073098ffc0b4679f8"
  
}


variable "eks_security_group_id" {
  default = "sg-08b9e8625db52aa76"
  
}


########### Variable for eks cluster##########
######## variable for key name #########
variable "key" {
  description = "Enter Key name"
  default = "opsssashu"
}
variable "k8s_version" {
  description = "k8s version"
  default = "1.13"
 
}

variable "cluster_name" {
  description = "Cluster Name"
  default = "eks_cluster"
}

########## Variable for EKS worker launch configuration######
###### Instance Type
variable "instance_type_worker" {
  default = "t3a.large"
}

########## variable of eks worker autoscaling group ######
variable "desired_capacity" {
  description = "The number of EC2 instances that should be running in the group"
  default = "3"
}

variable "max_size" {
  description = "The maximum size of the auto scale group"
  default = "5"
}

variable "min_size" {
  description = "The minimum size of the auto scale group"
  default = "3"
}
