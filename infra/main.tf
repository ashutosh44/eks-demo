################# Region of Infra #################
provider "aws" {
    version                 = "~> 2.0"
    region                  = "${var.infra_region}"
    shared_credentials_file = "${var.key_credentials_path}"
    profile                 = "ashu"
}


########### Craeting EKS cluster 
module "eks" {
  source                        = "../eks_cluster"
  k8s_version                   = "${var.k8s_version}"
  cluster_name                  = "${var.cluster_name}"
  security_group_ids            = "${var.eks_security_group_id}"
  subnet_ids                    = ["${var.eks_pvt_subnet_az_a}","${var.eks_pvt_subnet_az_b}"]
  instance_key                  = "${var.key}"
  instance_type                 = "${var.instance_type_worker}"
  security_groups               = ["${var.eks_security_group_id}"]
  worker_subnet                 = ["${var.eks_pvt_subnet_az_a}","${var.eks_pvt_subnet_az_b}"]
  desired_capacity              = "${var.desired_capacity}"
  max_size                      = "${var.max_size}"
  min_size                      = "${var.min_size}"
  cluster_name                  = "${var.env}_${var.cluster_name}"
  env                           = "${var.env}"
  
}
