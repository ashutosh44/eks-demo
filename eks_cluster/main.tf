#
# EKS Cluster Resources
#  * IAM Role to allow EKS service to manage other AWS service
#  * EKS Cluster
#

#IAM Role
resource "aws_iam_role" "cluster-role" {
  name = "ab_dev_cluster_iam_role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "eks-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = "${aws_iam_role.cluster-role.name}"
}

resource "aws_iam_role_policy_attachment" "eks-cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = "${aws_iam_role.cluster-role.name}"
}


######## EKS cluster #########
resource "aws_eks_cluster" "eks-cluster" {
  version              = "${var.k8s_version}"
  name                 = "${var.cluster_name}"
  role_arn             = "${aws_iam_role.cluster-role.arn}"
  enabled_cluster_log_types = ["api", "audit","authenticator","controllerManager","scheduler"]


  vpc_config {
    security_group_ids = ["${var.security_group_ids}"]
    subnet_ids         = ["${var.subnet_ids}"]
  }

  depends_on = [
    "aws_iam_role_policy_attachment.eks-cluster-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.eks-cluster-AmazonEKSServicePolicy",
  ]
}
